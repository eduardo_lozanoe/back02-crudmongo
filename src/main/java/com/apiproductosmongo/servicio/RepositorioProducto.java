package com.apiproductosmongo.servicio;

import com.apiproductosmongo.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String> {
    public List<Producto> findByPrecioBetween(double min, double max);

    @Query("{$and :[ {'precio' : {$gte: ?0} }, {'precio' : {$lte: ?1}} ]}")
    public List<Producto> findByPrecioRango(double min, double max);

    @Query("{'proveedores' : {$elemMatch : {'nombre':?0}}}")
    public List<Producto> findByProviders(String nombreProveedor);
}