package com.apiproductosmongo.servicio.impl;

import com.apiproductosmongo.servicio.ServicioProducto;
import com.apiproductosmongo.modelo.Producto;
import com.apiproductosmongo.servicio.RepositorioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpStatus;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    @Autowired
    RepositorioProducto repositorioProducto;

    @Override
    public void addProduct(Producto p) {
        this.repositorioProducto.insert(p);
    }

    @Override
    public Producto findById(String id) {
        Optional<Producto> r = this.repositorioProducto.findById(id);
        return r.isPresent()?r.get() : null;
    }

    @Override
    public List<Producto> findAll() {
        return this.repositorioProducto.findAll();
    }

    @Override
    public ResponseEntity updateProducto(Producto producto) {
        Optional<Producto> pr = this.repositorioProducto.findById(producto.getId());
        if (pr.isPresent()) {
            this.repositorioProducto.save(producto);
            return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity saveProductoById(String idProducto, Producto producto) {
        this.repositorioProducto.save(producto);
        producto.setId(idProducto);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity delete(Producto producto) {
        try {
            this.repositorioProducto.delete(producto);
            return new ResponseEntity<>("Producto eliminado correctamente.", HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>("No se ha podido eliminar el producto, intente más tarde.", HttpStatus.NO_CONTENT);
        }
    }

    public List<Producto> findByPrice(double min, double max){
        return this.repositorioProducto.findByPrecioBetween(min, max);
    }

    public List<Producto> findByPrecioRango(double min, double max){
        System.out.println("Si esta pasando por aca");
        return this.repositorioProducto.findByPrecioRango(min, max);
    }

    @Override
    public List<Producto> findByProvider(String nombreProveedor) {
        return this.repositorioProducto.findByProviders(nombreProveedor);
    }
}